﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using WorkWork.Core.ApplicationServices.Implementations;
using WorkWork.Core.Data.Dummy;
using WorkWork.Core.Scheduling;
using WorkWork.Core.Settings;

namespace WorkWork.ExampleApplication
{
    class Program
    {
        static void Main(string[] args)
        {
            var scheduler = new WorkScheduler(
                new DummyWorkService(), 
                new DummyWorkMaster(new CommonSettings()), 
                new CommonSettings());

            scheduler.Initialize();
            scheduler.Start();

            Console.ReadLine();
        }
    }
}
