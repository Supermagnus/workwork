﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using WorkWork.Core.ApplicationServices.Implementations;
using WorkWork.Core.ApplicationServices.Interfaces.Interfaces;
using WorkWork.Core.Domain;
using WorkWork.Core.Scheduling;
using WorkWork.Core.Settings;
using WorkWork.Tests.Mocked;

namespace WorkWork.Tests.Core
{
    [TestClass]
    public class WorkSchedulerTests
    {
        [TestMethod]
        public void TestMethod1()
        {
            var work = CreateWork("*/5 * * * *");
            var scheduler = new WorkScheduler(
                new MockedWorkService(work).Object,
                new DummyWorkMaster(), new CommonSettings());

            scheduler.Initialize();
            scheduler.Start();
        }


        private Work CreateWork(string expression)
        {
            return  new Work()
            {
                ConcurrentWorkLimit = 2,
                CronExpression = expression,
                Data = "<root></root>",
                Id = "test"
            };
        }
    }
}
