﻿using System.Collections;
using Moq;
using WorkWork.Core.Data.Interfaces;
using WorkWork.Core.Domain;

namespace WorkWork.Tests.Mocked
{
    public class MockedWorkService
    {
        public MockedWorkService(Work workToReturn)
        {
            var dummyService = new Mock<IWorkService>(MockBehavior.Default);

            dummyService
                .Setup(x => x.Get(It.IsAny<string>()))
                .Returns(workToReturn)
                .Callback((string id) => GetCallback = id);
            dummyService
                .Setup(x => x.GetAll())
                .Returns(new []{workToReturn});

            Object = dummyService.Object;
        }

        public IWorkService Object { get; private set; }
        public string GetCallback { get; private set; }

    }
}
