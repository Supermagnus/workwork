﻿using Common.Utilities.Settings;

namespace WorkWork.Core.Settings
{
    public class CommonSettings : ConfigSettingsBase
    {
        public int NumberOfThreads { get; set; }
    }
}
