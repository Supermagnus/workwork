﻿using System;
using System.Diagnostics;
using System.Threading;
using NCrontab;
using WorkWork.Core.ApplicationServices.Interfaces;
using WorkWork.Core.Domain;
using WorkWork.Core.Scheduling.Utils;

namespace WorkWork.Core.Scheduling
{
    public class Worker
    {
        private readonly CrontabSchedule _schedule;
        private readonly IWorkMaster _workMaster;
        private readonly Timer _timer;

        public Counter Counter { get; private set; }
        public Guid Id { get; set; }
        public Work Work { get; private set; }



        public Worker(Work work, CrontabSchedule schedule, IWorkMaster workMaster)
        {
            _schedule = schedule;
            _workMaster = workMaster;
            Id = Guid.NewGuid();
            Work = work;

          _timer = new Timer(state => Execute(),null,schedule.TimeToNextOccurrence(), Timeout.InfiniteTimeSpan);
            
        }

        public void Start()
        {
            Counter = new Counter(0);
            _timer.Change(NextExecutionTime(), new TimeSpan(0, 0, 0, 0, -1));
        }

        public void Pause()
        {
            Counter = new Counter(Work.ConcurrentWorkLimit);
        }

        private void Execute()
        {
            if(Counter.Value >= Work.ConcurrentWorkLimit )
                return;

           
            _timer.Change( NextExecutionTime(),new TimeSpan(0,0,0,0,-1));
            _workMaster.Execute(Work);
        }

        private TimeSpan NextExecutionTime()
        {
            var nextExecution = _schedule.TimeToNextOccurrence();
            //Console.WriteLine(string.Format("Executing {0} in {1}ms", Work.Id, nextExecution.TotalMilliseconds));
            return nextExecution;
        }

    }
}