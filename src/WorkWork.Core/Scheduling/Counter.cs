using System.Threading;

namespace WorkWork.Core.Scheduling
{
    /// <summary>
    /// A thread safe counter
    /// </summary>
    public class Counter
    {
        private int _counter;

        public Counter()
        {
            _counter = 0;
        }

        public Counter(int value)
        {
            _counter = value;
        }
        public int Value
        {
            get { return _counter; }
        }

        public void Increment()
        {
            Interlocked.Increment(ref _counter);
        }
        public void Decrement()
        {
            Interlocked.Decrement(ref _counter);
        } 

    }
}