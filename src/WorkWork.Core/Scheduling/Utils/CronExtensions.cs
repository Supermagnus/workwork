﻿using System;
using NCrontab;

namespace WorkWork.Core.Scheduling.Utils
{
    public static class CronExtensions
    {
        public static TimeSpan TimeToNextOccurrence(this CrontabSchedule schedule)
        {
            var datetime = schedule.GetNextOccurrence(DateTime.UtcNow);
            return datetime - DateTime.UtcNow;
        }
    }
}
