﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using NCrontab;
using WorkWork.Core.ApplicationServices.Interfaces;
using WorkWork.Core.ApplicationServices.Interfaces.Implementations;
using WorkWork.Core.ApplicationServices.Interfaces.Interfaces;
using WorkWork.Core.Data.Interfaces;
using WorkWork.Core.Domain;
using WorkWork.Core.Settings;

namespace WorkWork.Core.Scheduling
{
    public class WorkScheduler : IWorkScheduler
    {
        private readonly IWorkService _workService;
        private readonly IWorkMaster _workMaster;
        private readonly CommonSettings _commonSettings;
        private IDictionary<string, CrontabSchedule> _cronSchedules;
        private IDictionary<string, Worker> _workers;
        
        public WorkScheduler(IWorkService workService, IWorkMaster workMaster, CommonSettings commonSettings)
        {
            _workService = workService;
            _workMaster = workMaster;
            _commonSettings = commonSettings;
            
        }
        
        public void Initialize()
        {
            
            var works = _workService.GetAll();
            _cronSchedules = works.ToDictionary(
                key => key.Id,
                schedule => CrontabSchedule.Parse(schedule.CronExpression, new CrontabSchedule.ParseOptions(){IncludingSeconds = true}));
            _workers = works.ToDictionary(
                key => key.Id,
                work => new Worker(work ,_cronSchedules[work .Id], _workMaster));
        }

        public void Start()
        {
            foreach (var worker in _workers.Values)
            {
                worker.Start();
            }
        }

        public void Pause()
        {
            foreach (var worker in _workers.Values)
            {
                worker.Pause();
            }
        }

        public void Stop()
        {
            throw new NotImplementedException();
        }

        public IList<Work> CurrentExecutingWorks()
        {
            throw new NotImplementedException();
        }


    }

}
