﻿using WorkWork.Core.Domain;

namespace WorkWork.Core.ApplicationServices.Interfaces
{
    /// <summary>
    /// Think TaskMaster - responible for Work execution
    /// </summary>
    public interface IWorkMaster
    {
        void Execute(Work work);
        void Execute(string workId);
    }
}