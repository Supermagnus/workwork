﻿using System.Collections.Generic;
using WorkWork.Core.Domain;

namespace WorkWork.Core.ApplicationServices.Interfaces.Interfaces
{
    public interface IWorkScheduler
    {
        /// <summary>
        /// Initializes the scheduler and resets all work
        /// </summary>
        void Initialize();

        void Start();
        void Pause();
        void Stop();

        IList<Work> CurrentExecutingWorks();
    }

  
}
