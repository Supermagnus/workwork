﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkWork.Core.ApplicationServices.Interfaces;
using WorkWork.Core.ApplicationServices.Interfaces.Implementations;
using WorkWork.Core.ApplicationServices.Interfaces.Interfaces;
using WorkWork.Core.Domain;
using WorkWork.Core.Settings;

namespace WorkWork.Core.ApplicationServices.Implementations
{
    public class DummyWorkMaster :IWorkMaster
    {

        private readonly TaskFactory _taskFactory;
        private readonly CommonSettings _commonSettings;
        
        public DummyWorkMaster(CommonSettings commonSettings)
        {
            _commonSettings = commonSettings;
            _taskFactory = new TaskFactory(new LimitedConcurrencyLevelTaskScheduler(_commonSettings.NumberOfThreads));
        }

        public void Execute(Work work)
        {
            _taskFactory.StartNew(() => Console.WriteLine(DateTime.Now.ToString("T") +": Executing " + work.Id));

        }

        public void Execute(string workId)
        {
            throw new NotImplementedException();
        }
    }
}
