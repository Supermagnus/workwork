﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WorkWork.Core.Data.Interfaces;
using WorkWork.Core.Domain;

namespace WorkWork.Core.Data.Dummy
{
    public class DummyWorkService : IWorkService

    {
        public IList<Work> GetAll()
        {
            return new Work[]
            {
                new Work()
                {
                    ConcurrentWorkLimit = 2,
                    CronExpression = "*/2 * * * * *",
                    Id = "*/2"
                }, 
                 new Work()
                {
                    ConcurrentWorkLimit = 1,
                    CronExpression = "*/5 * * * * *",
                    Id = "*/5"
                }, 
                new Work()
                {
                    ConcurrentWorkLimit = 1,
                    CronExpression = "*/8 * * * * *",
                    Id = "*/8"
                }, 
            };
        }

        public Work Get(string id)
        {
            throw new NotImplementedException();
        }

        public string SaveOrUpdate(Work work)
        {
            throw new NotImplementedException();
        }

        public void Delete(string id)
        {
            throw new NotImplementedException();
        }
    }
}
