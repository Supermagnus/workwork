﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using WorkWork.Core.Domain;

namespace WorkWork.Core.Data.Interfaces
{
    public interface IWorkService
    {
        IList<Work> GetAll();
        Work Get(string id);
        string SaveOrUpdate(Work work);
        void Delete(string id);

    }
}
