﻿using System;

namespace WorkWork.Core.Domain
{
    /// <summary>
    /// Describes a work and everything related to it
    /// </summary>
    public class Work
    {
        /// <summary>
        /// Identifier for this work
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// When/How often this work should be executing
        /// </summary>
        public string CronExpression { get; set; }

        /// <summary>
        /// Number of instances of this work that can be running at the same time
        /// </summary>
        public int ConcurrentWorkLimit { get; set; }

        /// <summary>
        /// Configuration data
        /// </summary>
        public string Data { get; set; }
    }

}
